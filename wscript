#!/usr/bin/env python
# encoding: utf-8

import os
import sys

from waflib import Logs, Options, TaskGen

VERSION='1'
APPNAME='teamer'

# these variables are mandatory ('/' are converted automatically)
top = '.'
out = 'build'

auto_options = []
g_maxlen = 40
extra_cflags = [
    '-fipa-pure-const',
    '-fstrict-overflow'
]
warning_cflags = [
    '-Wall',
    '-Wconversion',
    '-Winline',
    '-Wmissing-declarations',
    '-Wshadow',
    '-Wsign-compare',
    '-Wsign-conversion',
    '-Wstrict-overflow=5',
    '-Wsuggest-attribute=const',
    '-Wsuggest-attribute=pure',
    '-Wtype-limits',
    '-pedantic'
]

def options(opt):
    opt.load('compiler_c')
    opt.load('gnu_dirs')

def configure(conf):
    conf.load('compiler_c')
    conf.load('gnu_dirs')

    conf.env.append_unique('CFLAGS', '-std=gnu11')
    conf.env.append_unique('CFLAGS', extra_cflags)
    conf.env.append_unique('CFLAGS', warning_cflags)

    conf.define('_GNU_SOURCE', 1)
    conf.define('PACKAGE', APPNAME)
    conf.define('VERSION', VERSION)

    conf.check_cfg(package='gop-3', uselib_store='GOP',
                   args='--cflags --libs')

def build(bld):
    bld.program(
        source       = ['log.c',
                        'main.c',
                        'print-version.c',
                        'rng.c'],
        target       = 'teamer',
        use          = ['GOP'],
        install_path = bld.env['BINDIR']
    )
