/*
 * This file is part of teamer.
 * 
 * Copyright (C) 2015 Karl Linden <karl.j.linden@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

#if HAVE_CONFIG_H
# include <config.h>
#endif /* HAVE_CONFIG_H */

#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#include <gop.h>

#include "common.h"
#include "log.h"
#include "print-version.h"
#include "rng.h"

static char **       names     = NULL;
static unsigned int  names_len = 0;

static char *        loglevel  = NULL;

static void
names_free(void)
{
    for (unsigned int i = 0; i < names_len; ++i) {
        free(names[i]);
    }
    free(names);
    names = NULL;
    names_len = 0;
    return;
}

static int NONNULL
names_add(const char * const name)
{
    char *       copy;
    unsigned int new_len;
    char **      new;

    copy = strdup(name);
    if (copy == NULL) {
        log_fatal_errno("could not allocate memory");
        return 1;
    }

    new_len = names_len + 1;
    new = realloc(names, new_len * sizeof(char *));
    if (new == NULL) {
        free(copy);
        log_fatal_errno("could not allocate memory");
        return 1;
    }

    new[names_len] = copy;
    names     = new;
    names_len = new_len;

    return 0;
}

static char * RETURNS_NONNULL
names_pop(const unsigned int n)
{
    assert(n < names_len);

    char * const name = names[n];
    names_len--;
    names[n] = names[names_len];

    return name;
}

static char * RETURNS_NONNULL
names_pop_rand(void)
{
    unsigned int rand = (unsigned int)rng_max((long)(names_len-1));
    return names_pop(rand);
}

static gop_return_t NONNULL
loglevel_cb(gop_t * const gop)
{
    log_level_t level = log_level_from_string(loglevel);
    if (level == LOG_LEVEL_NONE) {
        log_error("%s is not a valid loglevel", loglevel);
        gop_set_exit_status(gop, EXIT_FAILURE);
        return GOP_DO_EXIT;
    }
    log_set_level(level);
    return GOP_DO_CONTINUE;
}

static gop_return_t NONNULL
quiet_cb(gop_t * const gop)
{
    log_set_quiet(1);
    return GOP_DO_CONTINUE;
}

static unsigned int NONNULLA(1)
strtoui(const char * const nptr, char ** const endptr, const int base)
{
    unsigned long ul;
    ul = strtoul(nptr, endptr, base);
    if (ul > UINT_MAX) {
        errno = ERANGE;
        ul = UINT_MAX;
    }
    return (unsigned int)ul;
}

static int
read_file(char * filename)
{
    int     retval = 1;
    FILE *  file   = stdin;
    char *  line   = NULL;
    size_t  size   = 0;
    ssize_t ret;

    if (filename != NULL && strcmp(filename, "-") != 0) {
        file = fopen(filename, "r");
        if (file == NULL) {
            log_fatal_errno("could not open %s for reading", filename);
            goto error;
        }
    }

    for (;;) {
        errno = 0;
        ret = getline(&line, &size, file);
        if (ret < 0) {
            if (errno == 0) {
                break;
            } else {
                log_fatal_errno("could not read line");
                goto error;
            }
        }
        if (line[ret-1] == '\n') {
            ret--;
            line[ret] = '\0';
        }
        if (names_add(line)) {
            goto error;
        }
    }

    retval = 0;
error:
    if (file != stdin && file != NULL) {
        if (fclose(file)) {
            log_error_errno("could not close %s", filename);
        }
    }
    free(line);

    return retval;
}

int NONNULL
main(int argc, char ** argv)
{
    int exit_status = EXIT_FAILURE;

    unsigned int n;

    const gop_option_t options[] = {
        {"loglevel", 'l', GOP_STRING, &loglevel, &loglevel_cb,
            "fatal|error|warn|info|debug"},
        {"quiet", 'q', GOP_NONE, NULL, &quiet_cb,
            "do not print log messages", NULL},
        {"version", 'v', GOP_NONE, NULL, &print_version,
            "print version and exit", NULL},
        GOP_TABLEEND
    };

    gop_t * const gop = gop_new();
    if (gop == NULL) {
        goto error;
    }

    gop_set_program_name(gop, PACKAGE);
    gop_add_usage(gop, "N [FILE] [OPTIONS...]");
    gop_description(gop, "Randomize lines in FILE in groups of N and "
                         "N-1.\n"
                         "\n"
                         "With no FILE, or when FILE is -, read "
                         "standard input.");
    gop_add_table(gop, "Program options:", options);
    rng_add_gop_table(gop);
    gop_autohelp(gop);
    gop_parse(gop, &argc, argv);
    gop_destroy(gop);

    if (argc != 2 && argc != 3) {
        log_fatal("invalid number of arguments");
        goto error;
    }

    char * endptr;
    errno = 0;
    n = strtoui(argv[1], &endptr, 0);
    if (errno != 0) {
        log_fatal_errno("could not convert N");
        goto error;
    } else if (*endptr != '\0') {
        log_fatal("could not convert N: string not fully converted");
        goto error;
    }

    if (n == 0) {
        log_fatal("N must not be 0");
        goto error;
    }

    if (read_file(argv[2])) {
        goto error;
    }

    /* The names should now be printed in groups of the teams. The
     * desired team size is n, but it is not always possible to split
     * the number of names in teams all containing n names, thus let
     * a few teams have n - 1 members. Let N be the number of names
     * (names_len). Then
     * N = n*x + (n-1)*y
     * where x is the number of teams with n names and y is the number
     * of teams with n - 1 names. x should be as big as possible and y
     * should be as small as possible. The equation can be arranged to
     * the following
     * N = n*(x+y) - y
     * and it is known that
     * y(mod n) = -N(mod n),
     * which gives the smallest y to
     * 0 if N(mod n) = 0 and n - N(mod n) if N(mod n) != 0. x can then
     * be calculated by doing x = (N - (n-1)*y)/n. */
    unsigned int x;
    unsigned int y;
    unsigned int mod;
    mod = names_len % n;
    if (mod != 0) {
        y = n - mod;
    } else {
        y = 0;
    }
    if ((n-1)*y > names_len) {
        log_fatal("cannot divide input in groups of %d and %d", n, n-1);
        goto error;
    }
    x = (names_len - (n-1)*y)/n;

    unsigned int team;
    if (x) {
        team = n;
    } else {
        team = n - 1;
    }

    rng_seed();
    while (x || y) {
        assert(names_len > 0);
        char * const name = names_pop_rand();
        puts(name);
        free(name);

        team--;
        if (team == 0) {
            if (x) {
                x--;
            } else /* if (y) */ {
                y--;
            }
            if (x || y) {
                putchar('\n');
                if (x) {
                    team = n;
                } else /* if (y) */{
                    team = n - 1;
                }
            }
        }
    }

    exit_status = EXIT_SUCCESS;
error:
    names_free();

    return exit_status;
}
